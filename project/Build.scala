import sbt.Keys._
import sbt._


object BabylonBuild extends Build {

  val coreSettings = Seq(
    version := "1.0",
    scalaVersion := "2.11.8",
    resolvers ++= Seq(
      Resolver.mavenLocal
    ),
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor"  % akkaV,
      "org.json4s" % "json4s-jackson_2.11" % "3.3.0"
    )
  )


  val akkaV = "2.3.9"
  val scraperSettings = Seq(
    libraryDependencies ++= Seq(
      "com.gravity" % "goose" % "2.2-SNAPSHOT", // https://github.com/dr3s/goose
      "org.jsoup" % "jsoup" % "1.8+",
      "commons-validator" % "commons-validator" % "1.5+"
    )
  )

  val sprayV = "1.3.3"
  val restApiSettings = Seq(
    libraryDependencies ++= Seq(
      "org.apache.lucene" % "lucene-core" % "6.1.0",
      "org.apache.lucene" % "lucene-analyzers-common" % "6.1.0",
      "org.apache.lucene" % "lucene-queryparser" % "6.1.0",
      "org.apache.lucene" % "lucene-memory" % "6.1.0",
      "io.spray"            %%  "spray-can"     % sprayV,
      "io.spray"            %%  "spray-routing" % sprayV,
      "io.spray"            %%  "spray-testkit" % sprayV  % "test",
      "com.typesafe.akka"   %%  "akka-testkit"  % akkaV   % "test",
      "org.specs2"          %%  "specs2-core"   % "2.3.11" % "test"
    )
  )

  lazy val root = RootProject(file("."))

  lazy val scraper = Project("scraper", file("scraper"))
    .settings(coreSettings ++ scraperSettings : _*)

  lazy val restApi = Project("restApi", file("restApi"))
    .settings(coreSettings ++ restApiSettings : _*)

}