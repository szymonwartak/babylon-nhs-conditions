#NHS Conditions Scraper and Server

##Scraper

- sbt "project scraper" run (currently scraping just 'C' pages and saving to data/conditions.json)

##Server (RestAPI)

Lucene index & spray server

- sbt "project restApi" run (currently serving c-conditions.json with all conditions starting with 'c')
- goto: http://localhost:8080/search?q=cancer
