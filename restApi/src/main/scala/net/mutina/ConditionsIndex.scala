package net.mutina

import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.document.{Field, TextField, Document}
import org.apache.lucene.index.{DirectoryReader, IndexWriterConfig, IndexWriter}
import org.apache.lucene.queryparser.classic.QueryParser
import org.apache.lucene.search.IndexSearcher
import org.apache.lucene.store.RAMDirectory
import org.json4s.jackson.Serialization

/**
 * create, populate and expose conditions to querying
 */
class ConditionsIndex {
  implicit private val formats = org.json4s.DefaultFormats
  private val conditionsData =
    Serialization.read[Map[String, Map[String, PageContent]]](new java.io.FileReader("data/c-conditions.json"))


  // pump data into (in memory) Lucene index
  private val store = new RAMDirectory()
  private val indexWriter = new IndexWriter(store, new IndexWriterConfig(new StandardAnalyzer()))
  private def addCondition(name: String, details: Map[String, PageContent]): Unit = {
    val doc = new Document()
    doc.add(new TextField("name", name, Field.Store.YES))
    details.foreach(d => doc.add(new TextField(d._1, d._2.content, Field.Store.YES)))
    indexWriter.addDocument(doc)
  }
  conditionsData.foreach(condition => addCondition(condition._1, condition._2))
  indexWriter.commit()


  // create searchers for REST API
  private val indexSearcher = new IndexSearcher(DirectoryReader.open(store))
  private val parser = new QueryParser("intro", new StandardAnalyzer())

  def search(term: String) = {
    indexSearcher
      // overweight exact name matches and to a lesser extent partial name matches
      .search(parser.parse(s"""name:"${term}"^10 name:${term}^3 intro:${term}"""), 100)
      .scoreDocs
      .map(s => (s.score, indexSearcher.doc(s.doc)))
  }


}
