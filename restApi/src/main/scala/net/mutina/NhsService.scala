package net.mutina

import akka.actor.Actor
import org.json4s.jackson.Serialization
import spray.http.MediaTypes._
import spray.routing._

import scala.collection.JavaConversions._


case class PageContent(url: String, content: String)


class NhsServiceActor extends Actor with NhsService {
  def actorRefFactory = context

  def receive = runRoute(myRoute)
}

// this trait defines our service behavior independently from the service actor (for easier testing)
trait NhsService extends HttpService {
  implicit private val formats = org.json4s.DefaultFormats

  val conditions = new ConditionsIndex

  // spray-can REST routes defined
  val myRoute =
    path("") {
      get {
        respondWithMediaType(`text/html`) {
          complete {
            <html><body>
              usage:
              <ul>
                <li>/search?q=[search terms]</li>
              </ul>
            </body></html>
          }
        }
      }
    } ~
    // search takes 1 parameter and returns max 100 results in json format (name and intro only) with the score
    path("search") {
      get {
        parameters('q) { (q) =>
          respondWithMediaType(`application/json`) {
            complete {
              val res = conditions.search(q)
              println(s"query ${q} yields ${res.length} results:\n${
                res.map(x => (x._1, x._2.get("name"))).sortBy(-_._1).map(x => s"\t${x}").mkString("\n")
              }}")
              Serialization.write(
                res.map(x =>
                  x._2.iterator().map(f => (f.name(), f.stringValue())).toMap[String, String] + ("score" -> x._1)
                ))
            }
          }
        }
      }
    }
}
