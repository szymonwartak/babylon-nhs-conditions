package net.mutina

import akka.actor.{Actor, ActorSystem}
import akka.util.Timeout
import scala.concurrent.duration._

import scala.collection.mutable


/**
 * Scraper actor that accumulates scrapes and calls the parse function that performs
 * the scrape and post-processing.
 */
class NhsScraperActor(system: ActorSystem, cleanup: => Unit) extends Actor {
  val process = "Process next url"
  var count = 0

  implicit val timeout = Timeout(3 seconds)
  context.system.scheduler.schedule(100 millis, 100 millis, self, process)
  val seen = mutable.Set.empty[String]
  val toProcess = mutable.Queue.empty[(String, String => Unit)]

  def receive: Receive = {
    case Scrape(url, parseFunction) =>
      println(s"added... $url")
      if (!seen.contains(url.toLowerCase))
        toProcess += ((url, parseFunction))
      seen += url.toLowerCase
    case `process` =>
      toProcess.dequeueFirst(_ => true) match {
        case None if count > 10 =>
          println(s"...shutting down. ${count} pages scraped.")
          cleanup
          system.shutdown()
        case Some((url, parseFunction)) =>
          try {
            println(s"site scraping... $url")
            parseFunction(url)
            count += 1
          } catch { case e =>
            e.printStackTrace()
            println(s"failed scrape, retrying. ${e.getMessage}\n${e.toString}")
            //self ! Scrape(url, parseFunction) // retry
          }
      }
  }

}