package net.mutina

import java.io.PrintWriter

import akka.actor.{Props, _}
import com.gravity.goose.Goose
import org.json4s.jackson.Serialization
import org.jsoup.Jsoup

import scala.collection.mutable
import scala.language.postfixOps


// Messages
case class Scrape(url: String, parseFunction: String => Unit)

// condition data
case class PageContent(url: String, content: String)

/**
 * Steps in scrape:
 * 1) scrape a-z index pages to get list of conditions
 * 2) scrape condition (intro) pages for content and additional info
 * 3) scrape additional info (symptoms, etc) pages for content
 *
 * Contents are saved under conditions as flat text.
 */
object NhsScraper extends App {
  val goose = new Goose()

  implicit val formats = org.json4s.DefaultFormats
  val data = mutable.HashMap.empty[String, mutable.HashMap[String, PageContent]]

  val system = ActorSystem()
  val scraper = system.actorOf(Props(new NhsScraperActor(system, cleanup)))

  def cleanup(): Unit = {
    val out = new PrintWriter("data/conditions.json")
    out.println(Serialization.write(data))
    out.close()
  }

  // for each letter, get and scrape
  val indices = "0-9" :: ('a' to 'z').map(_.toString).toList
  indices
    .tail.tail.tail.take(1) // TODO: remove for production
    .foreach(i => scraper ! Scrape(s"http://www.nhs.uk/Conditions/Pages/BodyMap.aspx?Index=${i}", parseLetter))

  def parseLetter(url: String): Unit = {
//    val url = s"http://www.nhs.uk/Conditions/Pages/BodyMap.aspx?Index=A"
    val response = Jsoup.connect(url).ignoreContentType(true).execute()

    val linkR = """<a href="(/conditions/[^"]+)">""".r
    val matches = linkR.findAllIn(response.body).toSet
    println(s"parseLetter (${url}): found ${matches.size} matches")
    matches
//      .take(3) // TODO: remove for production
      .map( _ match {
      case linkR(conditionUrl) =>
        scraper ! Scrape(conditionUrl.toLowerCase, parseCondition)
    })
  }

  def parseCondition(urlPath: String): Unit = {
//    val urlPath = """/conditions/addisons-disease"""
    val conditionNameR = """/conditions/(.*)""".r
    val conditionName = urlPath match { case conditionNameR(conditionName) => conditionName }
    val url = s"http://www.nhs.uk${urlPath}"
    val response = Jsoup.connect(url).ignoreContentType(true).execute()

    val page = goose.extractContent(url, response.body)
    outputPageContent(conditionName, url, "intro", page.cleanedArticleText)

    // get subpages
    val subLinkR = s"""<a href="(${urlPath}/[^"]+)">""".toLowerCase.r
    val subLinkNameR = """.*/(.*)\.aspx.*""".r

    val matches = subLinkR.findAllIn(response.body.toLowerCase)
      .map(_.takeWhile(_ != '#').toLowerCase)
      .filter(!_.toLowerCase.contains("introduction.aspx")).toSet
    println(s"subpages - found. ${urlPath}. ${matches.size} matches\n${matches.map(x => s"\t${x}").mkString("\n")}")
    matches.map( _ match {
      case subLinkR(conditionUrl) =>
        conditionUrl match {
          case subLinkNameR(detailType) =>
            scraper ! Scrape(conditionUrl, parseConditionDetails(conditionName, detailType))
        }
      case x => println(s"ERROR: mismatch for matched url: ${x}")
    })
  }

  def parseConditionDetails(conditionName: String, detailType: String)(urlPath: String): Unit = {
    val url = s"http://www.nhs.uk${urlPath}"
    val page = goose.extractContent(url)
    outputPageContent(conditionName, url, detailType, page.cleanedArticleText)
  }

  def outputPageContent(conditionName: String, url: String, title: String, content: String): Unit = {
    val conditionData = data.getOrElseUpdate(conditionName, mutable.HashMap.empty[String, PageContent])
    conditionData += title -> PageContent(url, content)
  }
}


